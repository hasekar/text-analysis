package com.datascience.opennlp.demo;

import com.datascience.opennlp.demo.test.openNLP;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(DemoApplication.class, args);

        String sentence = "Electronic data for chromatography systems are not archived in the QC laboratory to ensure adherence to data integrity principles";


        openNLP namedEntityRecognization = new openNLP();

        namedEntityRecognization.nameFinder();

        namedEntityRecognization.PartsOfSpeech(sentence);

        namedEntityRecognization.SentenceDetection("src/main/resources/files/TextTobeAnalysed");
    }
}

package com.datascience.opennlp.demo.test;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.Span;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class openNLP {

    String File = new String();

    String sentence[] = new String[]{
            "Pierre",
            "Vinken",
            "is",
            "61",
            "years",
            "old",
            "."
    };

    String str = "";


    public void nameFinder() throws FileNotFoundException {

        try (InputStream modelIn = new FileInputStream("src/main/resources/models/en-ner-person.bin")){

            TokenNameFinderModel model = new TokenNameFinderModel(modelIn);

            NameFinderME nameFinder = new NameFinderME(model);
            Span nameSpans[] = nameFinder.find(sentence);
            for(Span x : nameSpans)
                System.out.println(x.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void PartsOfSpeech(String sentence) {

        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream("src/main/resources/models/en-pos-maxent.bin");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        POSModel model = null;
        try {
            model = new POSModel(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        POSTaggerME tagger = new POSTaggerME(model);

        WhitespaceTokenizer whitespaceTokenizer= WhitespaceTokenizer.INSTANCE;
        String[] tokens = whitespaceTokenizer.tokenize(sentence);

        String[] tags = tagger.tag(tokens);

        POSSample sample = new POSSample(tokens, tags);

        System.out.println(sample.toString());

//        System.out.println("\n");
//        for(String s : sample.getTags())
//            System.out.print(s + " + ");

        System.out.println("\n");


        for(int i = 0; i < tags.length; i++){
            if(tags[i].equals("NN") || tags[i].equals("NNS") || tags[i].equals("VB")|| tags[i].equals("RB") || tags[i].equals("NNP") || tags[i].equals("NN")) {
//                if((i+1) == tags.length)
//                    str += tokens[i] +" ("+tags[i]+")";
                str += tokens[i] + " + ";
            }
        }

        str += "\n";

        System.out.println(str);


        System.out.println("\n");




    }


    public void SentenceDetection(String fileName) throws IOException {

        List<String> sentence = readFile(fileName);

        InputStream inputStream = new FileInputStream("src/main/resources/models/en-sent.bin");
        SentenceModel model = new SentenceModel(inputStream);

        SentenceDetectorME detector = new SentenceDetectorME(model);
        sentence.forEach(line -> {

            String sentences[] = detector.sentDetect(line);

            for (String sent : sentences) {
                System.out.println("-----------------");
                System.out.println(sent);
                PartsOfSpeech(sent);
                System.out.println("-----------------");
            }

            try {
                writeConcept();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }

    public void writeConcept()
            throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("src/main/resources/files/concept.txt"));
        writer.write(str);
        writer.close();
    }


    List<String> readFile(String fileName) throws IOException {
        List<String> sentence = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sentence.add(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sentence;
        } finally {
            br.close();
        }
    }

}

Quality review and approval of deviations did not ensure thorough assessment of potential product quality risk by both contract acceptor (XYZ) and contract giver (ABC).
During cGMP drug substance intermediate manufacturing of PRD, lot# xxxx; XYZ performed unauthorized reprocessing due to particulate contamination in chromatographic purification fractions.
The contamination was identified as rubber particles from vacuum tubing.
XYZ classified the issue as a minor deviation without impact on product quality.
Per the Quality Agreement (QAG), this should have been classified as a significant deviation requiring notification to the customer.
Also, per the QAG, reprocessing is defined as unacceptable in case of re-filtration at any step though specifically to remove known foreign matter or contamination outside established limits.
The final product quality assessment was based on routine release testing.
However, since no further analytics were performed, there was no evidence that reprocessing was capable of removing potential extractables from the rubber particles exposed to organic solvent.
Furthermore, it remained unclear whether or not the vacuum tubing was included in any cleaning validation or was in contact with other products prior to use in this manufacturing.
It was also not plausible how the rubber particles could enter the Buchner flask against the airflow direction of the applied vacuum.
A potential overrun of the solvent’s boiling point, resulting in flushing the particles into to Buchner flask was not discussed in the root-case analysis performed by XYZ.
ABC became aware of this issue during batch record review and neither challenged the categorization of the deviation based on the product quality impact assessment, nor the unauthorized re-filtration performed by XYZ.
ABC released the material for parenteral use in phase 2 clinical trials.
Sufficient controls are not exercised over computers or related systems to assure unauthorized changes to control records and data.
Specifically, Your firm does not have proper controls in place to prevent manipulation of your laboratory's electronic raw data and prevent omissions in the data.
Your firm did not implement interim measures for equipment involved in raw material testing.
Your firm implemented interim measures for critical equipment involved in drug substance testing, drug product testing, and stability testing, however did not include an evaluation of equipment used for testing raw materials.
For example, two (2) FT-IR's [EN: xxx and EN:xxx] do not have controls for ensuring that all generated/acquired data is saved.
Additionally, I observed that an analyst can decide where data files are stored and can delete data locally, which affects which files are backed-up on the server.
The FT-IR's are used to conduct identity testing for release of raw material.
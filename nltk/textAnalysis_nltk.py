
# coding: utf-8

# In[11]:


import nltk
import nltk.data
from nltk import pos_tag, word_tokenize
#nltk.download('vader_lexicon')
from nltk.sentiment.vader import SentimentIntensityAnalyzer


# In[12]:



#READ FROM FILE
TEXT_ANALYSIS_FILE_NAME = "TextAnalytics.txt"
file= open(TEXT_ANALYSIS_FILE_NAME, "r")
file_content =  file.read()


# In[13]:


#SHOW FILE CONTENT
print file_content

file.close()

# In[14]:


#GET SENTENCES FROM INPUT TEXT
# import nltk.data
SENTENCE_OUTPUT_FILE_NAME = 'sentense.txt'

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
file = open(TEXT_ANALYSIS_FILE_NAME, "r")
file_content =  file.read()
# print '\n'.join(tokenizer.tokenize(data))
out = open(SENTENCE_OUTPUT_FILE_NAME,"w+r")
out.write('\n'.join(tokenizer.tokenize(file_content)))

print out.read()

out.close()


# In[15]:


# import nltk
# nltk.download('vader_lexicon')
# from nltk.sentiment.vader import SentimentIntensityAnalyzer 

    
    
NEGATIVE_PHRASE_SENTENCE_NLTK = 'negativePhraseSentence_nltk.txt'
SENTENCE_OUTPUT_FILE_NAME = 'sentense.txt'


try:  
    sentenceFile = open(SENTENCE_OUTPUT_FILE_NAME, "r")
    negativePhrase_nltk = open(NEGATIVE_PHRASE_SENTENCE_NLTK,"w")

    sid = SentimentIntensityAnalyzer()

    line = sentenceFile.readline()
    cnt = 1
    while line:
        list_of_words = line.split()        
        try:
            negatedWord = list_of_words[list_of_words.index('not') + 1]
            negativePhrase_nltk.write(line)
            print 'Found Negated Word'
        except ValueError:
            ss = sid.polarity_scores(line)
            if(ss['neg'] >= ss['pos'] ):
                print 'negative sentiment score'
                negativePhrase_nltk.write(line)
            else:
                print 'positive sentiment score'
        line = sentenceFile.readline()
        cnt += 1

finally:  
    sentenceFile.close()
    negativePhrase_nltk.close()


# In[16]:


# POS using NLTK

# import nltk
# from nltk import pos_tag, word_tokenize


CONCEPT2_OUTPUT_FILE_NAME = 'concepts.txt'
SENTENCE_OUTPUT_FILE_NAME = 'negativePhraseSentence_nltk.txt'


try:  
    sentenceFile = open(SENTENCE_OUTPUT_FILE_NAME, "r")
    conceptFile_nltk = open(CONCEPT2_OUTPUT_FILE_NAME,"w")

    line = sentenceFile.readline()
    cnt = 1
    
    while line:
        text = word_tokenize(line)
        list = nltk.pos_tag(text)
        count = len(list);  
        concept = ''
        for i in list:
            count = count - 1
            if(i[1]== 'VBD' or i[1]== 'NN' or i[1]== 'NNS' or i[1]== 'RB' or i[1]== 'VB' or i[1]== 'VBN'):
                if(count != 0):
                    concept = concept + i[0] + ' + '
                else:
                    concept = concept + i[0] 
        print list
        print '\n' + concept + '\n'
        conceptFile_nltk.write(concept+'\n')

        line = sentenceFile.readline()
        cnt += 1

finally:  
    sentenceFile.close()
    conceptFile_nltk.close()


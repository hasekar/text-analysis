const dataTreeSimple = {
  'result': [

    {
      'id': '1',
      'description': 'Biology'
      // 'parent': 'Molecular Biology'
    },
    {
      'id': '2',
      'description': 'biology faces',
      'parent': '1'
    },
    {
      'id': '3',
      'description': 'computational biology',
      'parent': '1'
    },
    {
      'id': '4',
      'description': 'Data organization',
      'parent': '1'
    },
    {
      'id': '5',
      'description': 'foreseeable future',
      'parent': '4'
    },
    {
      'id': '6',
      'description': 'database development and integration',
      'parent': '3'
    },
    {
      'id': '7',
      'description': 'specialized databases',
      'parent': '3'
    },
    {
      'id': '8',
      'description': 'new techniques',
      'parent': '3'
    }
    ],


};

export default dataTreeSimple;

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-concept-viewer-base',
  templateUrl: './concept-viewer-base.component.html',
  styleUrls: ['./concept-viewer-base.component.css']
})
export class ConceptViewerBaseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

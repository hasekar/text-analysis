import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConceptViewerBaseComponent } from './concept-viewer-base.component';

describe('ConceptViewerBaseComponent', () => {
  let component: ConceptViewerBaseComponent;
  let fixture: ComponentFixture<ConceptViewerBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConceptViewerBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConceptViewerBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

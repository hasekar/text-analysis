import { Component, OnInit } from '@angular/core';
import {ObservationConcept} from '../entityDefinition/observation-concept';
import {ObservationService} from '../service/observationService/observation.service';
import {ObservationChangeService} from '../service/observationService/observationChangeService/observation-change.service';
import {InputPayloadService} from '../service/inputPayloadChangeservice/input-payload.service';

@Component({
  selector: 'app-observation-input',
  templateUrl: './observation-input.component.html',
  styleUrls: ['./observation-input.component.css']
})
export class ObservationInputComponent implements OnInit {

  observationRecord = new ObservationConcept(-1, '', '', '', '');
  computedObservationRecord: ObservationConcept;
  inputString: string;
  constructor(private observationService: ObservationService, private observationChangeService: ObservationChangeService, private inputPayloadservice: InputPayloadService) { }

  ngOnInit() {
    this.observationChangeService.currentObservation.subscribe(observation => this.computedObservationRecord = observation);
    this.inputPayloadservice.currentInputPayload.subscribe(input => this.inputString = input);
  }

  CheckObservationRecord(recordID: any) {
    this.observationService.getObservation(recordID).subscribe(observation => {
      this.observationRecord = observation;
      this.inputString = observation.observation;
      this.inputPayloadservice.changeinputPayload(this.inputString);
      console.log(observation);
    }, error => {
      this.observationRecord = new ObservationConcept(0, '', '', '', '');
      console.log(error);
    });
  }

  inputPayloadValue(inputObservationText: string) {
    this.inputString = inputObservationText;
    this.inputPayloadservice.changeinputPayload(inputObservationText);
  }


  computeObservationConcept(recordID: any) {
    this.observationService.getComputeObservation(recordID).subscribe(computedObservation => {
      this.computedObservationRecord = computedObservation;
      this.observationChangeService.changeObservation(computedObservation);
      console.log(computedObservation);
    }, error => {
      this.computedObservationRecord = new ObservationConcept(0, '', '', '', '');
      this.observationChangeService.changeObservation(new ObservationConcept(0, '', '', '', ''));
      console.log(error);
    });
  }
}

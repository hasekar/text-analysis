export class ObservationConcept {
  recordId: number;
  observation: string;
  spaCy: string;
  nltk: string;
  openNLP: string;
  graphNodes: GainNode[];


  constructor(recordId: number, observation: string, spaCy: string, nltk: string, openNLP: string) {
    this.recordId = recordId;
    this.observation = observation;
    this.spaCy = spaCy;
    this.nltk = nltk;
    this.openNLP = openNLP;
  }
}

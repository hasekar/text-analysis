import { Component, OnInit } from '@angular/core';
import {ObservationConcept} from '../entityDefinition/observation-concept';
import {ObservationChangeService} from '../service/observationService/observationChangeService/observation-change.service';
import {InputPayloadService} from '../service/inputPayloadChangeservice/input-payload.service';
import {ObservationService} from '../service/observationService/observation.service';
import {ConceptKeyWord} from '../entityDefinition/ConceptKeyWord';

@Component({
  selector: 'app-observation-output',
  templateUrl: './observation-output.component.html',
  styleUrls: ['./observation-output.component.css']
})
export class ObservationOutputComponent implements OnInit {

  computedObservationRecord: ObservationConcept = new ObservationConcept(-1, '', '', '', '');
  inputString: string;
  keyWords: [];
  isSpinner: any = false;

  constructor(private observationChangeService: ObservationChangeService, private inputPayloadService: InputPayloadService, private observationservice: ObservationService) { }

  ngOnInit() {
    this.observationChangeService.currentObservation.subscribe(observation => this.computedObservationRecord = observation);
    this.inputPayloadService.currentInputPayload.subscribe(input => {
      this.inputString = input;
    });
  }

  postInputPayloadToBackend() {
    this.keyWords = [];
    this.isSpinner = true;
    this.observationservice.postInput(this.inputString).subscribe(output => {
      console.log(output);
      this.keyWords = output;
      this.isSpinner = false;
    }, error1 => {
      console.log(error1);
      this.isSpinner = false;
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservationOutputComponent } from './observation-output.component';

describe('ObservationOutputComponent', () => {
  let component: ObservationOutputComponent;
  let fixture: ComponentFixture<ObservationOutputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservationOutputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservationOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

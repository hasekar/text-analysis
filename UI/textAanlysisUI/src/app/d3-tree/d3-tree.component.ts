import { Component, OnInit } from '@angular/core';
import {AngularD3TreeLibService} from 'angular-d3-tree';
import dataTreeSimple from 'src/assets/data-tree-simple';
import {ObservationChangeService} from '../service/observationService/observationChangeService/observation-change.service';
import {ObservationConcept} from '../entityDefinition/observation-concept';

@Component({
  selector: 'app-d3-tree',
  templateUrl: './d3-tree.component.html',
  styleUrls: ['./d3-tree.component.css']
})
export class D3TreeComponent implements OnInit {
  computedObservationRecord: ObservationConcept = new ObservationConcept(-1, '', '', '', '');
  data: any[];
  ngOnInit() {
    this.observationChangeService.currentObservation.subscribe(observation => this.computedObservationRecord = observation);
    this.data = dataTreeSimple.result;

  }


  constructor(private observationChangeService: ObservationChangeService, private treeService: AngularD3TreeLibService) {
    this.data = dataTreeSimple.result;
  }

  nodeUpdated(node: any) {
    // console.info("app detected node change");
  }
  nodeSelected(node: any) {
    // console.info("app detected node selected", node);
  }

}

import { Component, OnInit } from '@angular/core';
import * as ChartConst from 'ng6-o2-chart';

@Component({
  selector: 'app-test-d3',
  templateUrl: './forceDirected-d3.component.html',
  styleUrls: ['./forceDirected-d3.component.css']
})
export class ForceDirectedD3Component {

  configData: any;
  treeMapDataJson: any;
  treeDataJson: any;
  forceDataJson: any;

  treeTypeName: string;
  forceTypeName: string;

  constructor() {
    // this.treeMapTypeName     = ChartConst.TREE_MAP_CHART_TYPE_NAME;
    this.treeTypeName     	= ChartConst.TREE_CHART_TYPE_NAME;
    this.forceTypeName     	= ChartConst.FORCE_CHART_TYPE_NAME;

    this.initilizeData();
  }


  private initilizeData() {
    // ConfigData = this.httpClient.get('assets/json/ConfigData.json');
    this.configData = {
      // tslint:disable-next-line:quotemark
      "className": {
        'axis': 'axis',
        'axisXBorder': 'axis_x',
        'axisXText': 'axis-x-text',
        'bar': 'bar',
        'barValue': 'bar-value',
        'line': 'line',
        'multiLinePrefix': 'line-',
        'grid': 'grid',
        'pie': 'pie',
        'pieInnerTitle': 'pie-inner-title',
        'pieInnerRadius': 'total',
        'histogram': 'histogram',
        'histogramBar': 'histogram-bar',
        'treemap': 'treemap',
        'treemapLabel': 'treemap-label',
        'packlayout': 'packlayout',
        'packlayoutLabel': 'packlayout-label',
      },
      'label': {
        'display': true,
      },
      'title': {
        'display': true,
        'name': 'Title',
        'className': 'chart-title',
        'height': 30,
        'leftMargin': -20,
        'bottomMargin': 10
      },
      'maxValue': {
        'auto': true,
        'x': 100,
        'y': 100,
      },
      'legend': {
        'display': true,
        'position':           'right',
        'totalWidth': 80,
        'initXPos': 5,
        'initYPos': 10,
        'rectWidth': 10,
        'rectHeight': 10,
        'xSpacing': 2,
        'ySpacing': 2
      },
      'color': {
        'auto': true,
        'defaultColorNumber': 10,
        'opacity': 1.0,
        'userColors': [
          'blue',
          'red',
          'green',
          'yellow',
          'PaleGoldenrod',
          'Khaki',
          'DarkKhaki',
          'Gold',
          'Cornsilk',
          'BlanchedAlmond',
          'Bisque',
          'NavajoWhite',
          'Wheat',
          'BurlyWood',
          'Tan',
          'RosyBrown',
          'SandyBrown',
          'Goldenrod',
          'DarkGoldenrod',
          'Peru',
          'Chocolate'
        ],
        'focusColor': 'red',
      },
      'pie': {
        'innerRadius': {
          'percent': 20,
          'title': 'Total'
        },
        'value': {
          'display': true,
        },
        'percent':{
          'display': false,
        }
      },
      'line': {
        'legend': 'lineEnd',
        'interpolate' : 'linear',
      },
      'grid': {
        'x': {
          'display': true,
        },
        'y': {
          'display': true,
        },
      },
      'margin': {
        'top': 30,
        'left': 30,
        'right': 10,
        'bottom': 20,
        'between': 5
      },
      'axis': {
        'rotation': 0,
        'borderLineWidth': 1,
        'xLabel': {
          'leftMargin': 0,
          'bottomMargin': 5
        },
        'yLabel': {
          'leftMargin': 0,
          'bottomMargin': 0
        },
      },
      'animation': {
        'enable': true,
        'duration': 4000,
      },
    };

    this.treeDataJson = {
        'name': 'Eve',
        'children': [
          { 'name': 'Cain'
          },
          {
            'name': 'Seth',
            'children': [
              { 'name': 'Enos' },
              { 'name': 'Noam' }
            ]
          },
          { 'name': 'Abel'
          },
          {
            'name': 'Awan',
            'children': [
              { 'name': 'Enoch' }
            ]
          },
          { 'name': 'Azura'
          },
        ]
      };


    this.treeMapDataJson = {
      'name': 'Root',
      'children': [
        { 'name': 'Dir1', 'children': [
            { 'name': 'Dir2', 'children': [
                { 'name': 'FileA', value: 5000 },
                { 'name': 'FileB', value: 3000 },
                { 'name': 'Dir3', 'children': [
                    { 'name': 'FileC', value: 2000 },
                    { 'name': 'Dir4', 'children': [
                        { 'name': 'FileD', value: 1000 },
                        { 'name': 'FileE', value: 1500 }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }

    this.forceDataJson = {
        'groups': [
          {'id': 1, 'name': 'Hokkaido'},
          {'id': 2, 'name': 'Tohoku'},
          {'id': 3, 'name': 'Kanto'},
          {'id': 4, 'name': 'Chubu'},
          {'id': 5, 'name': 'kinki'},
          {'id': 6, 'name': 'Chugoku'},
          {'id': 7, 'name': 'Shikoku'},
          {'id': 8, 'name': 'Kyushu'},
        ],
        'nodes': [
          {'id': 'Sapporo', 'group': 1},
          {'id': 'Sendai', 'group': 2},
          {'id': 'Morioka', 'group': 2},
          {'id': 'Akita', 'group': 2},
          {'id': 'Fukushima', 'group': 2},
          {'id': 'Mito', 'group': 3},
          {'id': 'Utsunomiya', 'group': 3},
          {'id': 'Saitama', 'group': 3},
          {'id': 'Chiba', 'group': 3},
          {'id': 'Tokyo', 'group': 3},
          {'id': 'Kofu', 'group': 4},
          {'id': 'Nagano', 'group': 4},
          {'id': 'Niigata', 'group': 4},
          {'id': 'Toyama', 'group': 4},
          {'id': 'Kanazawa', 'group': 4},
          {'id': 'Fukui', 'group': 4},
          {'id': 'Shizuoka', 'group': 4},
          {'id': 'Nagoya', 'group': 4},
          {'id': 'Gifu', 'group': 4},
          {'id': 'Otsu', 'group': 5},
          {'id': 'Kyoto', 'group': 5},
          {'id': 'Osaka', 'group': 5},
          {'id': 'Kobe', 'group': 5},
          {'id': 'Nara', 'group': 5},
          {'id': 'Kyoto', 'group': 5},
          {'id': 'Tottori', 'group': 6},
          {'id': 'Hiroshima', 'group': 6},
          {'id': 'Matsue', 'group': 6},
          {'id': 'Matsuyama', 'group': 7},
          {'id': 'Tokushima', 'group': 7},
          {'id': 'Kochi', 'group': 7},
          {'id': 'Fukuoka', 'group': 8},
          {'id': 'Nagasaki', 'group': 8},
          {'id': 'Kumamoto', 'group': 8},
          {'id': 'Naha', 'group': 8},
        ],
        'links': [
          {'source': 'Sendai', 'target': 'Sapporo', 'value': 1},
          {'source': 'Morioka', 'target': 'Sapporo', 'value': 1},
          {'source': 'Akita', 'target': 'Sapporo', 'value': 1},
          {'source': 'Fukushima', 'target': 'Sapporo', 'value': 1},
          {'source': 'Morioka', 'target': 'Sendai', 'value': 10},
          {'source': 'Akita', 'target': 'Sendai', 'value': 10},
          {'source': 'Fukushima', 'target': 'Sendai', 'value': 10},
          {'source': 'Chiba', 'target': 'Tokyo', 'value': 20},
          {'source': 'Utsunomiya', 'target': 'Tokyo', 'value': 20},
          {'source': 'Mito', 'target': 'Tokyo', 'value': 20},
          {'source': 'Saitama', 'target': 'Tokyo', 'value': 30},
          {'source': 'Kofu', 'target': 'Tokyo', 'value': 30},
          {'source': 'Nagano', 'target': 'Tokyo', 'value': 30},
          {'source': 'Naha', 'target': 'Tokyo', 'value': 30},
          {'source': 'Osaka', 'target': 'Tokyo', 'value': 40},
          {'source': 'Sendai', 'target': 'Tokyo', 'value': 40},
          {'source': 'Hiroshima', 'target': 'Tokyo', 'value': 20},
          {'source': 'Shizuoka', 'target': 'Nagoya', 'value': 10},
          {'source': 'Tokyo', 'target': 'Nagoya', 'value': 40},
          {'source': 'Osaka', 'target': 'Nagoya', 'value': 40},
          {'source': 'Kyoto', 'target': 'Nagoya', 'value': 40},
          {'source': 'Kyoto', 'target': 'Osaka', 'value': 30},
          {'source': 'Hiroshima', 'target': 'Osaka', 'value': 20},
          {'source': 'Toyama', 'target': 'Kanazawa', 'value': 10},
          {'source': 'Fukui', 'target': 'Kanazawa', 'value': 10},
          {'source': 'Niigata', 'target': 'Kanazawa', 'value': 10},
          {'source': 'Tottori', 'target': 'Kobe', 'value': 10},
          {'source': 'Tottori', 'target': 'Hiroshima', 'value': 10},
          {'source': 'Matsue', 'target': 'Hiroshima', 'value': 10},
          {'source': 'Matsuyama', 'target': 'Hiroshima', 'value': 10},
          {'source': 'Tokushima', 'target': 'Kochi', 'value': 10},
          {'source': 'Matsuyama', 'target': 'Kochi', 'value': 10},
          {'source': 'Nagasaki', 'target': 'Fukuoka', 'value': 10},
          {'source': 'Kumamoto', 'target': 'Fukuoka', 'value': 10},
          {'source': 'Naha', 'target': 'Fukuoka', 'value': 10},
        ]
      };

  }

}

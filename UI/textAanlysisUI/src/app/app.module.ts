import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {MatIconModule} from '@angular/material/icon';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatMenuModule} from '@angular/material/menu';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';

import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatDividerModule} from '@angular/material/divider';


import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';





import {MatTooltipModule} from '@angular/material/tooltip';
import {MatToolbarModule} from '@angular/material/toolbar';


import {MatStepperModule} from '@angular/material/stepper';
import {MatInputModule} from '@angular/material/input';
import {HttpClientModule} from '@angular/common/http';

import { AngularD3TreeLibModule } from 'angular-d3-tree';



import { AppComponent } from './app.component';
import { BaseComponent } from './base/base.component';
import { TitleComponent } from './title/title.component';
import { ConceptViewerBaseComponent } from './concept-viewer-base/concept-viewer-base.component';
import { ObservationInputComponent } from './observation-input/observation-input.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ObservationOutputComponent } from './observation-output/observation-output.component';
import { D3TreeComponent } from './d3-tree/d3-tree.component';
import { Ng6O2ChartModule } from 'ng6-o2-chart';
import { ForceDirectedD3Component } from './force-directed-d3/forceDirected-d3.component';


@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    TitleComponent,
    ConceptViewerBaseComponent,
    ObservationInputComponent,
    ObservationOutputComponent,
    D3TreeComponent,
    ForceDirectedD3Component
  ],
  imports: [
    BrowserModule,
    MatIconModule,
    MatGridListModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatRadioModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatTabsModule,
    HttpClientModule,
    AngularD3TreeLibModule,
    MatButtonToggleModule,
    Ng6O2ChartModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatCardModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

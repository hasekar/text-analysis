import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ObservationConcept} from '../../entityDefinition/observation-concept';
import {ResourceService} from '../resource.service';
import {Observable} from 'rxjs';
import {ConceptKeyWord} from '../../entityDefinition/ConceptKeyWord';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ObservationService {
  private currentObservationConcept: ObservationConcept;
  constructor(private _http: HttpClient, private resourceService: ResourceService) { }

  private observationConceptURL =  this.resourceService.HOST_NAME + this.resourceService.OBSERVATION_GET;

  private observationConceptComputeURL =  this.resourceService.HOST_NAME + this.resourceService.OBSERVATION_COMPUTE;

  private observationInput = this.resourceService.HOST_NAME + this.resourceService._INPUT


  /** GET Observation from the local host */
  getObservation (recordId: number): Observable<ObservationConcept> {
    return this._http.get<ObservationConcept>(`${this.observationConceptURL}/${recordId}`);
  }

  getComputeObservation (recordId: number): Observable<ObservationConcept> {
    return this._http.get<ObservationConcept>(`${this.observationConceptComputeURL}/${recordId}`);
  }

  /** POST: send input payload to backend service*/
  postInput (input: string): Observable<ConceptKeyWord> {
    return this._http.post<ConceptKeyWord>(`${this.observationInput}`, input, httpOptions);
  }
}

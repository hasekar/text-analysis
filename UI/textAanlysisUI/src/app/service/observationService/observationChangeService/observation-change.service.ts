import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {ObservationConcept} from '../../../entityDefinition/observation-concept';

@Injectable({
  providedIn: 'root'
})
export class ObservationChangeService {

  private currentObservationSource = new BehaviorSubject<ObservationConcept>(new ObservationConcept(-1, '', '', '', ''));
  currentObservation = this.currentObservationSource.asObservable();

  constructor() { }

  changeObservation(customer: ObservationConcept) {
    this.currentObservationSource.next(customer);
  }
}

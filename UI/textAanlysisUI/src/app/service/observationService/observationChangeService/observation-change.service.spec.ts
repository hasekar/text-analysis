import { TestBed } from '@angular/core/testing';

import { ObservationChangeService } from './observation-change.service';

describe('ObservationChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ObservationChangeService = TestBed.get(ObservationChangeService);
    expect(service).toBeTruthy();
  });
});

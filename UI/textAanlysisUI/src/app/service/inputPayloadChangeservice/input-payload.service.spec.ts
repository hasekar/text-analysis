import { TestBed } from '@angular/core/testing';

import { InputPayloadService } from './input-payload.service';

describe('InputPayloadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InputPayloadService = TestBed.get(InputPayloadService);
    expect(service).toBeTruthy();
  });
});

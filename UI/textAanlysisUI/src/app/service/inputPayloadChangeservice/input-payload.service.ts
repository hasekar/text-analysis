import { Injectable } from '@angular/core';
import {ObservationConcept} from '../../entityDefinition/observation-concept';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InputPayloadService {

  private currentInputPayloadSource = new BehaviorSubject<string>('');
  currentInputPayload = this.currentInputPayloadSource.asObservable();

  constructor() { }

  changeinputPayload(inputPayload: string) {
    this.currentInputPayloadSource.next(inputPayload);
  }
}

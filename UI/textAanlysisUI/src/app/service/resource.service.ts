import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {
  private PORT_NUMBER = 8080;
  HOST_NAME = 'http://localhost:' + this.PORT_NUMBER;
  OBSERVATION_GET = '/observationConcept/get';
  OBSERVATION_COMPUTE = '/observationConcept/compute';

  _INPUT = '/stanfordcorenlp/input'

  constructor() { }
}

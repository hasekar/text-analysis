package com.gene.textanalytics.controller;

import com.gene.textanalytics.entity.ConceptKepWord;
import com.gene.textanalytics.service.StanfordCoreNLPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST }, maxAge = 3600)
@RestController
@RequestMapping("stanfordcorenlp")
public class StanfordCoreNLP_Controller {

    @Autowired
    StanfordCoreNLPService stanfordCoreNLP;

    @PostMapping("/input")
    public List<ConceptKepWord> test(@RequestBody String payload) {
        System.out.println(payload);
        List<ConceptKepWord> conceptKepWords = stanfordCoreNLP.nerPipeLine(payload);
        System.out.println(conceptKepWords);
        return conceptKepWords;
    }
}

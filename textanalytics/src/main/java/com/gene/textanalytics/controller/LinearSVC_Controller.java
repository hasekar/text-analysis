package com.gene.textanalytics.controller;

import com.gene.textanalytics.entity.ConceptKepWord;
import com.gene.textanalytics.service.LinearSVC;
import com.gene.textanalytics.service.StanfordCoreNLPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST }, maxAge = 3600)
@RestController
@RequestMapping("linear_svc")
public class LinearSVC_Controller {

    @Autowired
    StanfordCoreNLPService stanfordCoreNLP;

    @Autowired
    LinearSVC linearSVC;

    @PostMapping("/input")
    public List<ConceptKepWord> test(@RequestBody String payload) throws IOException, InterruptedException {
        System.out.println(payload);
        List<ConceptKepWord> conceptKepWordsCoreNLP = stanfordCoreNLP.nerPipeLine(payload);
        linearSVC.generateConceptFromString(payload);
        List<ConceptKepWord> conceptKepWordsLinearSVC = linearSVC.getJsonList();
        return mergeAllConcepts(conceptKepWordsCoreNLP, conceptKepWordsLinearSVC);

    }

    private List<ConceptKepWord> mergeAllConcepts(List<ConceptKepWord> coreNLP, List<ConceptKepWord> linearSVC) {
        List<ConceptKepWord> concepts = new ArrayList<>(coreNLP);
        concepts.addAll(linearSVC);
        return concepts;
    }



}

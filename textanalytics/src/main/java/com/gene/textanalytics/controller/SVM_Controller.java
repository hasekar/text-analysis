package com.gene.textanalytics.controller;

import com.gene.textanalytics.entity.ConceptKepWord;
import com.gene.textanalytics.service.SVM;
import com.gene.textanalytics.service.StanfordCoreNLPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST }, maxAge = 3600)
@RestController
@RequestMapping("svm")
public class SVM_Controller {


    @Autowired
    StanfordCoreNLPService stanfordCoreNLP;

    @Autowired
    SVM svm;

    @PostMapping("/input")
    public List<ConceptKepWord> test(@RequestBody String payload) throws IOException, InterruptedException {
        System.out.println(payload);
        List<ConceptKepWord> conceptKepWordsCoreNLP = stanfordCoreNLP.nerPipeLine(payload);
        System.out.println(conceptKepWordsCoreNLP);
        svm.generateConceptFromString(payload);
        List<ConceptKepWord> conceptKepWordsLinearSVC = svm.getJsonList();
        return mergeConcepts(conceptKepWordsCoreNLP, conceptKepWordsLinearSVC);
    }

    private List<ConceptKepWord> mergeConcepts(List<ConceptKepWord> coreNLP, List<ConceptKepWord> svm) {
        List<ConceptKepWord> concepts = new ArrayList<>(coreNLP);
        concepts.addAll(svm);
        return concepts;
    }
}

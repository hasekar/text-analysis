package com.gene.textanalytics.controller;

import com.gene.textanalytics.entity.ConceptKepWord;
import com.gene.textanalytics.service.Etree;
import com.gene.textanalytics.service.StanfordCoreNLPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST }, maxAge = 3600)
@RestController
@RequestMapping("etree")
public class Etree_Controller {

    @Autowired
    StanfordCoreNLPService stanfordCoreNLP;

    @Autowired
    Etree etree;

    @PostMapping("/input")
    public List<ConceptKepWord> test(@RequestBody String payload) throws IOException, InterruptedException {
        System.out.println(payload);
        List<ConceptKepWord> conceptKepWordsCoreNLP = stanfordCoreNLP.nerPipeLine(payload);
        etree.generateConceptFromString(payload);
        List<ConceptKepWord> conceptKepWordsLinearSVC = etree.getJsonList();
        return mergeConcepts(conceptKepWordsCoreNLP, conceptKepWordsLinearSVC);
    }

    private List<ConceptKepWord> mergeConcepts(List<ConceptKepWord> coreNLP, List<ConceptKepWord> etree) {
        List<ConceptKepWord> concepts = new ArrayList<>(coreNLP);
        concepts.addAll(etree);
        return concepts;
    }
}

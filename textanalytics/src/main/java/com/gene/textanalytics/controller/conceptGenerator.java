package com.gene.textanalytics.controller;

import com.gene.textanalytics.entity.ObservationConcept;
import com.gene.textanalytics.exceptions.InvalidRecordIdException;
import com.gene.textanalytics.repository.ObservationConceptRepository;
import com.gene.textanalytics.repository.ObservationRepository;
import com.gene.textanalytics.service.OpenNLP;
import com.mongodb.client.FindIterable;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;


@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST }, maxAge = 3600)
@RestController
@RequestMapping("/concept")
public class conceptGenerator {

    @Autowired OpenNLP openNLP;

    @Autowired ObservationRepository observationRepository;

    @Autowired ObservationConceptRepository ObservationConceptRepository;

    private ObservationConcept observationConcept;


    @GetMapping("/openNLP")
    public String openNLP() throws IOException {


        FindIterable<Document> doc = observationRepository.findAllDocument();

        for(Document document: doc) {
            String concept = "";

            observationConcept = new ObservationConcept();

            List<String> sentences= openNLP.SentenceDetection(document.getString("Observation"));
            int recordID = document.getInteger("Record_ID");

            observationConcept.setRecordId(recordID);

            for(String sentence : sentences) {
                concept += openNLP.PartsOfSpeech(sentence);
            }

            observationConcept.setOpenNLP(concept);
            ObservationConceptRepository.save(observationConcept);

            System.out.println("Completed Document with Record ID: "+recordID );

        }

        return "Concept generated for all observation";
    }


    @GetMapping("openNLP/{recordID}")
    public ObservationConcept openNLP_convert(@PathVariable int recordID) throws IOException {

        Document document = observationRepository.findDocumentWithRecordId(recordID);

        if(document != null) {

            String concept = "";

            String originalObservation = document.getString("Observation");

            observationConcept = new ObservationConcept();

            List<String> sentences= openNLP.SentenceDetection(originalObservation);

            observationConcept.setRecordId(recordID);

            for(String sentence : sentences) {
                concept += openNLP.PartsOfSpeech(sentence);
            }

            observationConcept.setObservation(originalObservation);
            observationConcept.setOpenNLP(concept);
            ObservationConceptRepository.save(observationConcept);

            System.out.println("Completed Document with Record ID: "+recordID );

            return observationConcept;
        }

        else {
            throw new InvalidRecordIdException("Invalid Record ID: "+recordID);
        }
    }
}

package com.gene.textanalytics.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gene.textanalytics.entity.GraphNode;
import com.gene.textanalytics.entity.ObservationConcept;
import com.gene.textanalytics.exceptions.InvalidRecordIdException;
import com.gene.textanalytics.repository.ObservationConceptRepository;
import com.gene.textanalytics.repository.ObservationRepository;
import com.gene.textanalytics.service.Nltk;
import com.gene.textanalytics.service.OpenNLP;
import com.gene.textanalytics.service.SpaCy;
import com.mongodb.client.FindIterable;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST }, maxAge = 3600)
@RestController
@RequestMapping("observationConcept")
public class GetObservationConcept {

    @Autowired ObservationRepository observationRepository;

    @Autowired ObservationConceptRepository observationConceptRepository;

    @Autowired OpenNLP openNLP;

    @Autowired Nltk nltk;

    @Autowired SpaCy spaCy;



    @GetMapping("/get/{recordID}")
    public ObservationConcept getConceptWithRecordID(@PathVariable int recordID) {
        Optional<ObservationConcept> originalObservation = Optional.of(findOriginalObservationWithRecordID(recordID));
        ObservationConcept observationConcept = new ObservationConcept();;

        if(originalObservation.isPresent()) {
            observationConcept = originalObservation.get();
        }

        return observationConcept;

    }

    @GetMapping("/compute/all")
    public void runnall() throws IOException, InterruptedException {

        FindIterable<Document> doc = observationRepository.findAllDocument();

        ObservationConcept observationConcept =  new ObservationConcept();
        for(Document document: doc) {

            String concept = "";

            int recordID = document.getInteger("Record_ID");

            observationConcept = computeConceptWithRecordID(recordID);


            observationConceptRepository.save(observationConcept);

        }

    }

    @GetMapping("/compute/{recordID}")
    public ObservationConcept computeConceptWithRecordID(@PathVariable int recordID) throws IOException, InterruptedException {

        Optional<ObservationConcept> originalObservation = Optional.of(findOriginalObservationWithRecordID(recordID));

        ObservationConcept observationConcept = new ObservationConcept();;

        if(originalObservation.isPresent()) {

            Optional<ObservationConcept> observationConceptOptional = observationConceptRepository.findById(recordID);

            if(observationConceptOptional.isPresent()) {

                observationConcept = observationConceptOptional.get();
            } else {

                if(originalObservation.get().getObservation() == null) {
                    throw new InvalidRecordIdException("Invalid Observation");
                }

                observationConcept.setRecordId(originalObservation.get().getRecordId());
                observationConcept.setObservation(originalObservation.get().getObservation());

                setOpenNLP_Output(observationConcept);
                setNLTk_output(observationConcept);
                setSpacyOutPut(observationConcept);

                observationConceptRepository.save(observationConcept);
            }

        } else {
            throw new InvalidRecordIdException("Invalid Record ID: "+recordID);
        }

        observationConcept.setGraphNodes(getGraph());
        return observationConcept;
    }

    private void setOpenNLP_Output(ObservationConcept observationConcept) throws IOException {

        String openNLP_contextOutput = openNLP.generateConceptFromString(observationConcept.getObservation());
        observationConcept.setOpenNLP(openNLP_contextOutput);
    }

    private void setNLTk_output(ObservationConcept observationConcept) throws IOException, InterruptedException {

        String nltk_conceptOutput = nltk.generateConceptFromString(observationConcept.getObservation());
        observationConcept.setNltk(nltk_conceptOutput);
    }

    private void setSpacyOutPut(ObservationConcept observationConcept) throws IOException, InterruptedException {

        String spacy_conceptOutput = spaCy.generateConceptFromString(observationConcept.getObservation());
        observationConcept.setSpaCy(spacy_conceptOutput);
    }

    public ObservationConcept findOriginalObservationWithRecordID(int recordID) {

        ObservationConcept observationConcept;

        Document document = observationRepository.findDocumentWithRecordId(recordID);

        if(document != null) {

            observationConcept = new ObservationConcept();

            String sep = System.lineSeparator();
            String text = "This is line one." + sep + "This is line two.";

            observationConcept.setObservation(document.getString("Observation"));
            observationConcept.setRecordId(document.getInteger("Record_ID"));
            observationConcept.setSpaCy(text);
            observationConcept.setNltk("");
            observationConcept.setOpenNLP("");

            return observationConcept;

        } else {
            throw new InvalidRecordIdException("Invalid Record ID: "+recordID);
        }
    }

    @GetMapping("/get/graph")
    public List<GraphNode> getGraph() throws IOException {

        GraphNode graphNode = new GraphNode();

        ObjectMapper mapper = new ObjectMapper();

        // read JSON from a file
        Map<String, List<String>> jsonMap = mapper.readValue(new File(
                        "/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/graph_input.json"),
                new TypeReference<Map<String, List<String>>>(){});

        System.out.println("*** JSON File Contents ***");
//        Iterator<Map.Entry<String, List<String>>> entries = jsonMap.entrySet().iterator();
//        while (entries.hasNext()) {
//            Map.Entry<String, List<String>> entry = entries.next();
//            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue().get(0));
//        }

        List<GraphNode> graphNodeList = new ArrayList<>();


        List<String> rootChild = jsonMap.get("root");

        if(rootChild.isEmpty())
            return graphNodeList;

        for(String childnode : rootChild) {
            graphNodeList.add(new GraphNode(childnode,childnode,null));
        }

        graphNodeList = generateGraphNodes(jsonMap, "root",graphNodeList);

        graphNodeList.forEach(i->i.toString());

        return graphNodeList;
    }

    private List<GraphNode> generateGraphNodes(Map<String,List<String>> jsonMap, String node, List<GraphNode> graphNodeList) {


        List<String> child = jsonMap.get(node);

        if(child == null) {
            graphNodeList.add(new GraphNode(node,node,null));

            return graphNodeList;
        }

        child.forEach( childNode -> {
            System.out.println(childNode +"=>" + node );
            graphNodeList.add(new GraphNode(childNode,childNode,node));
            generateGraphNodes(jsonMap,childNode,graphNodeList);
        });

        return graphNodeList;
    }
}

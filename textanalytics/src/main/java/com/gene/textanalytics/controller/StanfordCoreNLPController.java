package com.gene.textanalytics.controller;

import com.gene.textanalytics.service.StanfordCoreNLPService;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = { "*" }, methods = { RequestMethod.GET, RequestMethod.POST }, maxAge = 3600)
@RestController
@RequestMapping("corenlp")
public class StanfordCoreNLPController {

    @Autowired
    StanfordCoreNLPService stanfordCoreNLP;

    @GetMapping("/trainmodel")
    public void trainModel() {

        String modePath;
        String propertyFilePath;
        String trainFilePath;

        modePath = "/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/stanfordCoreNLP/ner-model.ser.gz";
        propertyFilePath = "/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/stanfordCoreNLP/property.txt";
        trainFilePath = "/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/stanfordCoreNLP/standford_train.txt";

        stanfordCoreNLP.trainAndWrite(modePath, propertyFilePath, trainFilePath);

    }

    @GetMapping("/testmodel")
    public void testModel() {

        String modePath = "/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/stanfordCoreNLP/ner-model.ser.gz";

        CRFClassifier coreNLPModel = stanfordCoreNLP.getModel(modePath);

        String[] tests = new String[] {"John brought x360, which is from Hp", "Smith brought mac which is from Apple company at Michigan"};

        for (String item : tests) {
            doTagging(coreNLPModel, item);
        }
    }

    private void doTagging(CRFClassifier model, String input) {
        input = input.trim();
        System.out.println(input + "=>"  +  model.classifyToString(input));
    }


    @GetMapping("/test")
    public void test() {
        String input = "On 12/25/1209 John brought x360, This is ur email address saravanann@gmail.com from Apple company at Michigan";
        stanfordCoreNLP.nerPipeLine(input);
    }
}

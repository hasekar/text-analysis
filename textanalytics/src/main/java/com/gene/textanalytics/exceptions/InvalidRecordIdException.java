package com.gene.textanalytics.exceptions;

public class InvalidRecordIdException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public InvalidRecordIdException(String message) {
		super(message);
	}

}
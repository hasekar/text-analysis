package com.gene.textanalytics.service;

import com.gene.textanalytics.entity.ConceptKepWord;
import com.gene.textanalytics.entity.Label;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreEntityMention;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.sequences.SeqClassifierFlags;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;


@Service
public class StanfordCoreNLPService {

    static StanfordCoreNLP stanfordCoreNLP_pipeLine;

    @PostConstruct
    public void ServiceSetUp() {

        Properties props = new Properties();
        props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner");
        this.stanfordCoreNLP_pipeLine = new StanfordCoreNLP(props);
        System.out.println("=============Finished initilization=================");
    }

    public void trainAndWrite(String modelOutPath, String prop, String trainingFilepath) {
        Properties props = StringUtils.propFileToProperties(prop);
        props.setProperty("serializeTo", modelOutPath);
        if (trainingFilepath != null) {
            props.setProperty("trainFile", trainingFilepath);
        }
        SeqClassifierFlags flags = new SeqClassifierFlags(props);
        CRFClassifier<CoreLabel> crf = new CRFClassifier<>(flags);
        crf.train();
        crf.serializeClassifier(modelOutPath);
    }

    public CRFClassifier getModel(String modelPath) {
        return CRFClassifier.getClassifierNoExceptions(modelPath);
    }

    public void generateGraph() {
        String text = "A quick brown fox jumped over the lazy dog.";
        TreebankLanguagePack tlp = new PennTreebankLanguagePack();
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
        LexicalizedParser lp = LexicalizedParser.loadModel();
        lp.setOptionFlags(new String[]{"-maxLength", "500", "-retainTmpSubcategories"});
        TokenizerFactory<CoreLabel> tokenizerFactory =
                PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
        List<CoreLabel> wordList = tokenizerFactory.getTokenizer(new StringReader(text)).tokenize();
        Tree tree = lp.apply(wordList);
        GrammaticalStructure gs = gsf.newGrammaticalStructure(tree);
        Collection<TypedDependency> tdl = gs.typedDependenciesCCprocessed(true);

//        com.chaoticity.dependensee.Main
//        ImageIO.write(image, "jpg", new File("output.png"));
//        Main.writeImage(tree,tdl, "image.png",3);
    }

    public List<ConceptKepWord> nerPipeLine(String inputString) {

        List<ConceptKepWord> conceptKepWordList = new ArrayList<>();

        CoreDocument doc = new CoreDocument(inputString);
        stanfordCoreNLP_pipeLine.annotate(doc);
        System.out.println("---");
        System.out.println("entities found");
        for (CoreEntityMention em : doc.entityMentions())
            System.out.println("\tdetected entity: \t"+em.text()+"\t"+em.entityType());
        System.out.println("---");
       doc.entityMentions().forEach(token -> {

           if(token.entityType().equals("PERSON"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.PERSON));

           if(token.entityType().equals("EMAIL"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.EMAIL));

           if(token.entityType().equals("NUMBER"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.CODE));

           if(token.entityType().equals("DATE"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.DATE));

           if(token.entityType().equals("STATE_OR_PROVINCE"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.LOCATION));

           if(token.entityType().equals("TIME"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.TIME));

           if(token.entityType().equals("ORGANIZATION"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.ORGANIZATION));

           if(token.entityType().equals("SET"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.DATE));

           if(token.entityType().equals("RELIGION"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.RELIGION));

           if(token.entityType().equals("COUNTRY"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.COUNTRY));

           if(token.entityType().equals("NATIONALITY"))
               conceptKepWordList.add(new ConceptKepWord(token.text(), Label.NATIONALITY));

       });
       return conceptKepWordList;
    }
}

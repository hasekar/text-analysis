package com.gene.textanalytics.service;

import com.gene.textanalytics.entity.ConceptKepWord;
import com.gene.textanalytics.entity.JsonService;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Service
public class LinearSVC {

    public void generateConceptFromString(String inputString) throws IOException, InterruptedException {

        System.out.println("LinearSVC - Writing");
        writeToInputFile(inputString);
        System.out.println("LinearSVC - exec scripts");
        runNLTK_PythonScript();
//        System.out.println("LinearSVC - read output");
//        return readOutputConcept();
    }

    public void writeToInputFile(String inputString) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/TextAnalyticsInput.txt"));
        writer.write(inputString);
        writer.close();
    }

    public List<ConceptKepWord> getJsonList() throws FileNotFoundException {
        JsonService jsonService = new JsonService("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/output/linearSVCPrediction.json");
        return  jsonService.getJsonOutput();
    }

//    public String readOutputConcept() throws FileNotFoundException {
//
//        return new Scanner(new File("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/output/linearSVCPrediction.json")).useDelimiter("\\Z").next();
//    }

    public void runNLTK_PythonScript() throws InterruptedException {

        try {
            Process p = Runtime.getRuntime().exec("python /Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/LinearSVC_w2v.py");

            System.out.println("Waiting for python execution ...");

            p.waitFor();

            System.out.println("Python execution is done.");
        }
        catch (IOException e) {
            System.out.println("exception Details:");
            e.printStackTrace();
        }
    }
}

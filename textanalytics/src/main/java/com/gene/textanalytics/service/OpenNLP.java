package com.gene.textanalytics.service;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

@Service
public class OpenNLP {

    public String PartsOfSpeech(String sentence) {

        String conceptString = "";

        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream("src/main/resources/openNLPmodels/en-pos-maxent.bin");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        POSModel model = null;
        try {
            model = new POSModel(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        POSTaggerME tagger = new POSTaggerME(model);

        WhitespaceTokenizer whitespaceTokenizer= WhitespaceTokenizer.INSTANCE;
        String[] tokens = whitespaceTokenizer.tokenize(sentence);

        String[] tags = tagger.tag(tokens);


        for(int i = 0; i < tags.length; i++){
            if(tags[i].equals("NN") || tags[i].equals("NNS") || tags[i].equals("VB")|| tags[i].equals("RB") || tags[i].equals("NNP") || tags[i].equals("NN")) {
//                if((i+1) == tags.length)
//                    str += tokens[i] +" ("+tags[i]+")";
                conceptString += tokens[i] + " + ";
            }
        }

        conceptString += "\n";

        return conceptString;

    }


    public List<String> SentenceDetection(String inputString) throws IOException {

        InputStream inputStream = new FileInputStream("src/main/resources/openNLPmodels/en-sent.bin");

        SentenceModel model = new SentenceModel(inputStream);

        SentenceDetectorME detector = new SentenceDetectorME(model);

        String sentences[] = detector.sentDetect(inputString);

        return Arrays.asList(sentences);

    }


    public String generateConceptFromString(String observationString) throws IOException {

        String outputConcept = "";

        List<String> sentences= SentenceDetection(observationString);

        for(String sentence : sentences) {
            outputConcept += PartsOfSpeech(sentence);
        }

        return outputConcept;
    }

}

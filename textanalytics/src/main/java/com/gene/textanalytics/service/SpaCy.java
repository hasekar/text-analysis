package com.gene.textanalytics.service;

import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Scanner;

@Service
public class SpaCy {

    public String generateConceptFromString(String inputString) throws IOException, InterruptedException {

        System.out.println("Spacy - Writing");
        writeToInputFile(inputString);
        System.out.println("Spacy - exec scripts");
        runSpaCy_PythonScript();
        System.out.println("Spacy - read output");
        return readOutputConcept();

    }

    public void writeToInputFile(String inputString) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/TextAnalytics.txt"));
        writer.write(inputString);
        writer.close();
    }

    public String readOutputConcept() throws FileNotFoundException {

        return new Scanner(new File("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/concepts.txt")).useDelimiter("\\Z").next();
    }

    public void runSpaCy_PythonScript() throws InterruptedException {
        try {

            String systemOutput;
            Process p = Runtime.getRuntime().exec("python /Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/textAnalysis_spacy.py");

            System.out.println("Waiting for python execution ...");

            p.waitFor();
            System.out.println("Python execution is done.");

            System.out.println("Python execution is done.");
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(p.getErrorStream()));

            System.out.println("Here is the standard output of the command:\n");
            while ((systemOutput = stdInput.readLine()) != null) {
                System.out.println(systemOutput);
            }

            System.out.println("Here is the standard error of the command (if any):\n");
            while ((systemOutput = stdError.readLine()) != null) {
                System.out.println(systemOutput);
            }

        }
        catch (IOException e) {
            System.out.println("exception happened - here's what I know: ");
            e.printStackTrace();
        }
    }
}

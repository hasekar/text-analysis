package com.gene.textanalytics.service;

import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Scanner;

@Service
public class Nltk {

    public String generateConceptFromString(String inputString) throws IOException, InterruptedException {

        System.out.println("nltk - Writing");
        writeToInputFile(inputString);
        System.out.println("nltk - exec scripts");
        runNLTK_PythonScript();
        System.out.println("nltk - read output");
        return readOutputConcept();
    }

    public void writeToInputFile(String inputString) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/nltk/TextAnalytics.txt"));
        writer.write(inputString);
        writer.close();
    }

    public String readOutputConcept() throws FileNotFoundException {

        return new Scanner(new File("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/nltk/concepts.txt")).useDelimiter("\\Z").next();
    }

    public void runNLTK_PythonScript() throws InterruptedException {

        try {
            Process p = Runtime.getRuntime().exec("python /Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/nltk/textAnalysis_nltk.py");

            System.out.println("Waiting for python execution ...");

            p.waitFor();

            System.out.println("Python execution is done.");
        }
        catch (IOException e) {
            System.out.println("exception happened - here's what I know: ");
            e.printStackTrace();
        }
    }
}

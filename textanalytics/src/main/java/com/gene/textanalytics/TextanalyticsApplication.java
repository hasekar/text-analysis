package com.gene.textanalytics;

import com.gene.textanalytics.service.StanfordCoreNLPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TextanalyticsApplication implements CommandLineRunner {


	@Autowired
	StanfordCoreNLPService stanfordCoreNLPService;

	public static void main(String[] args) {
		SpringApplication.run(TextanalyticsApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
	}
}







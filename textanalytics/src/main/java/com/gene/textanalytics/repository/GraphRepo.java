package com.gene.textanalytics.repository;

import com.gene.textanalytics.entity.GraphNode;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface GraphRepo {
    @Repository
    public interface GraphRepository extends MongoRepository<GraphNode, String> {
    }

}

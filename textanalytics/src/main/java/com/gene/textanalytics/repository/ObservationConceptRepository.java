package com.gene.textanalytics.repository;

import com.gene.textanalytics.entity.ObservationConcept;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObservationConceptRepository extends MongoRepository<ObservationConcept, Integer> {
}

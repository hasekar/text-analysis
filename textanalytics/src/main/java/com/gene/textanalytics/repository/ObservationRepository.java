package com.gene.textanalytics.repository;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.stereotype.Repository;

import static com.mongodb.client.model.Filters.eq;

@Repository
public class ObservationRepository  {

    MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
    MongoDatabase database = mongoClient.getDatabase("observationDB2");

    MongoCollection<Document> collection = database.getCollection("observationRecord2");


    public Document findDocumentWithRecordId(int recordID) {
        FindIterable<Document> documents = collection.find(eq("Record_ID", recordID));
        return documents.first();
    }

//    public Document findDocumentById(int record_id) {
//        BasicDBObject query = new BasicDBObject();
//
//        query.put("price", new BasicDBObject("$eq", record_id));
//
//        Document document = (Document) collection.find(query);
//
//        return document;
//    }

    public FindIterable<Document> findAllDocument() {

         return collection.find();

    }

}
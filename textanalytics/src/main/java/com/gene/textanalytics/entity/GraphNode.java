package com.gene.textanalytics.entity;

import lombok.Data;

@Data
public class GraphNode {


    String id;
    String description;
    String parent;

    public GraphNode() {
    }

    public GraphNode(String id, String description, String parent) {
        this.id = id;
        this.description = description;
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "GraphNode{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", parent='" + parent + '\'' +
                '}';
    }
}

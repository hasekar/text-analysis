package com.gene.textanalytics.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
public class ObservationConcept {

    @Id
    private int recordId;
    private String observation;
    private String spaCy;
    private String nltk;
    private String openNLP;
    private List<GraphNode> graphNodes;

    public ObservationConcept() {
    }

}

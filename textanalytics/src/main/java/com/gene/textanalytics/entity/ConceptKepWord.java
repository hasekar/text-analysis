package com.gene.textanalytics.entity;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class ConceptKepWord {
    String keyWord;
    Label label;

    public ConceptKepWord(String keyWord, Label label) {
        this.keyWord = keyWord;
        this.label = label;
    }
}

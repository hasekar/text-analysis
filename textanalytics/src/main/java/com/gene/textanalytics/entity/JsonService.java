package com.gene.textanalytics.entity;

import lombok.Data;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Data
public class JsonService {

    private String jsonFileLocation;

    public JsonService(String jsonFileLocation) {
        this.jsonFileLocation = jsonFileLocation;
    }

    public List<ConceptKepWord> getJsonOutput() throws FileNotFoundException {

        JSONParser jsonParser = new JSONParser();

        List<ConceptKepWord> outputConceptList = new ArrayList<>();

        try (FileReader reader = new FileReader(jsonFileLocation))
        {
            Object obj = jsonParser.parse(reader);

            JSONObject jsonObject = (JSONObject) obj;

            outputConceptList = (List<ConceptKepWord>) jsonObject.get("output");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputConceptList;
    }
}

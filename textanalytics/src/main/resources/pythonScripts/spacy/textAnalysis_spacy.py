
# coding: utf-8

# In[7]:
from __future__ import unicode_literals, print_function

import nltk
import nltk.data
#nltk.download('vader_lexicon')
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import spacy 
from spacy import displacy
import json
import sys

reload(sys)
sys.setdefaultencoding('utf8')


#import plac
#import random
#from pathlib import Path
#import spacy

from tqdm import tqdm # loading bar

nlp = spacy.load('en_core_web_sm')



# In[8]:


#READ FROM FILE
TEXT_ANALYSIS_FILE_NAME = "/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/TextAnalytics.txt"
file= open(TEXT_ANALYSIS_FILE_NAME, "r")
file_content =  file.read()


# In[9]:


#SHOW FILE CONTENT
print (file_content)


# In[10]:



SENTENCE_OUTPUT_FILE_NAME = '/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/sentense.txt'

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
file = open(TEXT_ANALYSIS_FILE_NAME, "r")
file_content =  file.read()
# print '\n'.join(tokenizer.tokenize(data))
out = open(SENTENCE_OUTPUT_FILE_NAME,"w+r")
out.write('\n'.join(tokenizer.tokenize(file_content)))
output = out.read()
#print (output)
out.close();




NEGATIVE_PHRASE_SENTENCE_SPACY = '/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/negativePhrase_spacy.txt'
SENTENCE_OUTPUT_FILE_NAME = '/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/sentense.txt'


  
sentenceFile = open(SENTENCE_OUTPUT_FILE_NAME, "r")
negativePhrase_spacy = open(NEGATIVE_PHRASE_SENTENCE_SPACY,"w")

sid = SentimentIntensityAnalyzer()

line = sentenceFile.readline()
cnt = 1
while line:
    list_of_words = line.split()
    
    ss = sid.polarity_scores(line)
    if(ss['neg'] >= ss['pos'] ):
        print ('negative sentiment score')
        negativePhrase_spacy.write(line)
    else:
        print ('positive sentiment score')
        negativePhrase_spacy.write(line)

    line = sentenceFile.readline()
    cnt += 1


sentenceFile.close()
negativePhrase_spacy.close()


NEGATIVE_SENTENCE_OUTPUT_FILE_NAME = '/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/negativePhrase_spacy.txt'
CONCEPT2_OUTPUT_FILE_NAME = '/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/concepts.txt'

data = {}

try:  
    negativeSenntenceFile_spacy = open(NEGATIVE_SENTENCE_OUTPUT_FILE_NAME,"r")
    conceptFile_nltk = open(CONCEPT2_OUTPUT_FILE_NAME,"w")


    line = negativeSenntenceFile_spacy.readline()
    
    cnt = 1
    root_list = []
    while line:

        text = unicode(line)
        doc = nlp(text)

        concept = '';
        for token in doc:
            list = []
            if(token.is_stop != True and token.text != '\n'):
                if(token.dep_ == 'ROOT'):
                   root_list.append(token.text)
                if(token.tag_ == 'NNP' or token.tag_ == 'NN'  or token.tag_=='NN' or token.tag_ == 'CD' or token.dep_ != 'neg' or token.text != ' ' or token.text != 'i.e.' ):
                    if(token.tag_ != 'RB' and token.tag_ != 'PRP' and token.tag_ != 'VB' and token.tag_ != 'punct' and token.tag_ != 'CC' and token.tag_ != 'IN' and token.tag_ != 'DT' and token.tag_ != ':' and token.tag_ != ',' and token.text != '.' and token.text != '(' and token.text !=')' and token.text != ']' and token.text != '['):
                            concept = concept + token.text + ' + '
                            data[token.text] = [child.text for child in token.children if (child.is_stop != True and child.tag_ != 'RB' and child.tag_ != 'PRP' and child.tag_ != 'VB' and child.tag_ != 'punct' and child.tag_ != 'CC' and child.tag_ != 'IN' and child.tag_ != 'DT' and child.tag_ != ':' and child.tag_ != ',' and child.text != '.' and child.text != '(' and child.text !=')') and child.text != ']' and child.text != '[']

                if(token.text == 'not' or token.text == 'no' or token.text == 'negative'):
                    concept = concept + token.text + ' + '
                    data[token.text] = [child.text for child in token.children if (child.is_stop != True and child.tag_ != 'RB' and child.tag_ != 'PRP' and child.tag_ != 'VB' and child.tag_ != 'punct' and child.tag_ != 'CC' and child.tag_ != 'IN' and child.tag_ != 'DT' and child.tag_ != ':' and child.tag_ != ',' and child.text != '.' and child.text != '(' and child.text !=')')]

            print(token.text  + "("+token.tag_+")")

        conceptFile_nltk.write(concept+'\n')

        print ("----------------")
        print(root_list)
                            
        line = negativeSenntenceFile_spacy.readline()
        cnt += 1


    data['root'] = root_list;
    j = json.dumps(data)
    print(j)


finally:  
    negativeSenntenceFile_spacy.close()
    conceptFile_nltk.close()

f1 = open("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/pythonScripts/spacy/graph_input.json", 'w')
f1.write(json.dumps(data, sort_keys=True, indent=4, separators=(',', ': ')))
f1.close()


import numpy as np

with open('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/input/glove.6B.50d.txt', "rb") as lines:
    w2v = {line.split()[0]: np.array(map(float, line.split()[1:]))
           for line in lines}

class MeanEmbeddingVectorizer(object):
    def __init__(self, word2vec):
        self.word2vec = word2vec
        self.dim = len(word2vec.itervalues().next())

    def fit(self, X, y):
        return self

    def transform(self, X):
        return np.array([
            np.mean([self.word2vec[w] for w in words if w in self.word2vec]
                    or [np.zeros(self.dim)], axis=0)
            for words in X
        ])

meanEmbeddingVectorizer = MeanEmbeddingVectorizer(w2v)
print ('meanEmbeddingVectorizer is done')
print meanEmbeddingVectorizer


import pandas

colnames = ['No', 'Words', 'Classes']
AnalysisData = pandas.read_csv('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/AnalyseLabelTrainingData.csv', names=colnames)
EquipmentData = pandas.read_csv('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/EquipmentLabelTrainingData.csv', names=colnames)
LocationData = pandas.read_csv('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/LocationLabelTrainingData.csv', names=colnames)
SystemData = pandas.read_csv('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/SystemLabelTrainingData.csv', names=colnames)

AnalysisDataList = AnalysisData.Words.tolist()
EquipmentDataList = EquipmentData.Words.tolist()
LocationDataList = LocationData.Words.tolist()
SystemDataList = SystemData.Words.tolist()

print AnalysisDataList
print EquipmentDataList
print LocationDataList
print SystemDataList

updatedInputTrainingData = [AnalysisDataList, EquipmentDataList, LocationDataList, SystemDataList]

inputTrainingData = [['gowning room', 'room','laboratory', 'Basel', 'electric station', 'Michigan','San Jose'],
     ['GSP003', 'GSP006', 'GSP014', 'EE001', 'DP435'],
     ['data', 'text', 'number', 'raw', 'file', 'memory','computer']]
outputLabel = ['location', 'code', 'system']

print ('from trainning data file')


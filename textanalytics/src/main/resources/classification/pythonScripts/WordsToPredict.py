import nltk
import nltk.data
from nltk import pos_tag, word_tokenize
from nltk.sentiment.vader import SentimentIntensityAnalyzer 
from nltk.corpus import stopwords

#TEXT_ANALYSIS_FILE_NAME = 'TextAnalyticsInput.txt'
#SENTENCE_OUTPUT_FILE_NAME = 'sentense.txt'
#NEGATIVE_PHRASE_SENTENCE_NLTK = 'negativePhraseSentence_nltk.txt'

file= open('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/TextAnalyticsInput.txt', 'r')
file_content =  file.read()
print 'Text Analytics File -> ' + file_content
file.close()
# Sentence Formation - save in sentence.txt

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
file = open('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/TextAnalyticsInput.txt', 'r')
file_content =  file.read()
out = open('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/sentense.txt','w')
out.write('\n'.join(tokenizer.tokenize(file_content)))
print ('sentence Formed')
file.close()
out.close()

# Stop words initialization

stop_words = set(stopwords.words('english'))
stop_words.remove('not')
stop_words.remove('no')

wordsToPredict = []
negatedWordList = []

negativePhrase_nltk = open('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/negativePhraseSentence_nltk.txt','w')

sentenceFile = open('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/pythonScripts/sentense.txt', 'r')

# Initialize SentimentIntensityAnalyzer
sid = SentimentIntensityAnalyzer()
print 'Finding Negative sentence'
#print sentenceFile.read()
line = sentenceFile.readline()
cnt = 1
while line:

    print line

    foundnegatedWord = False;
    word_tokens  = line.split()
    print word_tokens
    index = 1
    wordListLength = len(word_tokens)

    ss = sid.polarity_scores(line)

    for w in word_tokens:
        if((w == 'not' or w == 'no') and index != wordListLength):
            print 'negated Sentence Found'
            negatedWordList.append(w + ' ' + word_tokens[index] )
            wordsToPredict.append([w + ' ' + word_tokens[index]])
            word_tokens.remove(word_tokens[index])
            foundnegatedWord = True;

        index = index + 1

        if(w not in stop_words):
            wordsToPredict.append([w])

    if(foundnegatedWord == True):
        negativePhrase_nltk.write(line)


    if(foundnegatedWord != True):
        if(ss['neg'] >= ss['pos'] ):
            print 'negative sentiment score'
            negativePhrase_nltk.write(line)
            for w in word_tokens:
                if(w not in stop_words):
                    wordsToPredict.append([w])

        else:
            print 'positive sentiment score'

    line = sentenceFile.readline()
    cnt += 1

negativePhrase_nltk.close()
sentenceFile.close()

print wordsToPredict


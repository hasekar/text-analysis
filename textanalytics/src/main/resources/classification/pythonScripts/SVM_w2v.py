import itertools
import json
from sklearn import svm
from sklearn.pipeline import Pipeline

from MeanEmbeddingVectorizer import meanEmbeddingVectorizer
from WordsToPredict import wordsToPredict
from TrainingData import inputTrainingData, outputLabel

svm_w2v = Pipeline([
    ("word2vec vectorizer", meanEmbeddingVectorizer),
    ("SVM", svm.SVC(kernel='linear'))])

svm_w2v.fit(inputTrainingData,outputLabel)

print wordsToPredict

svm = {}
svmPrediction = []
outputLabel = svm_w2v.predict(wordsToPredict)

for inputlist, label in itertools.izip(wordsToPredict, outputLabel):
    svm = {}
    
    wordsInputKeyWord = inputlist[0].split()
    
    if( len(wordsInputKeyWord) > 1 and wordsInputKeyWord[0] == 'not'):
        svm["keyWord"] = inputlist[0];
        svm["label"] = 'negated_word';
        svmPrediction.append(svm)
        print(inputlist[0] + ' - ' + 'negated_word')

    if(wordsInputKeyWord[0] != 'not' and wordsInputKeyWord[0] != 'no'):
        svm["keyWord"] = inputlist[0];
        svm["label"] = label;
        svmPrediction.append(svm)
        print(inputlist[0] + ' - ' + label)

outputJson = {}
outputJson['output'] = svmPrediction;

jsonOutput = json.dumps(outputJson, sort_keys=True, indent=4, separators=(',', ': '))
with open("/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/output/svmPrediction.json", 'w') as f:
    f.write(jsonOutput)
    


import itertools
import json
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline

from MeanEmbeddingVectorizer import meanEmbeddingVectorizer
from WordsToPredict import wordsToPredict
from TrainingData import inputTrainingData, outputLabel

LinearSVC_w2v = Pipeline([
    ("word2vec vectorizer", meanEmbeddingVectorizer),
    ("LinearSVC", LinearSVC(random_state=0, tol=1e-5))])

LinearSVC_w2v.fit(inputTrainingData,outputLabel)

print wordsToPredict

linearSVC = {}
linearSVCPrediction = []
outputLabel = LinearSVC_w2v.predict(wordsToPredict)

for inputlist, label in itertools.izip(wordsToPredict, outputLabel):
    linearSVC = {}
    
    wordsInputKeyWord = inputlist[0].split()
    
    if( len(wordsInputKeyWord) > 1 and wordsInputKeyWord[0] == 'not'):
        linearSVC['keyWord'] = inputlist[0];
        linearSVC['label'] = 'negated_word';
        linearSVCPrediction.append(linearSVC)
        print(inputlist[0] + ' - ' + 'negated_word')

    if(wordsInputKeyWord[0] != 'not' and wordsInputKeyWord[0] != 'no'):
        linearSVC['keyWord'] = inputlist[0];
        linearSVC['label'] = label;
        linearSVCPrediction.append(linearSVC)
        print(inputlist[0] + ' - ' + label)

outputJson = {}
outputJson['output'] = linearSVCPrediction;

jsonOutput = json.dumps(outputJson, sort_keys=True, indent=4, separators=(',', ': '))
with open('/Users/harishsekar/Documents/Project/DataScience/HTC_GLOBAL/textAnalysis/textanalytics/src/main/resources/classification/output/linearSVCPrediction.json', 'w') as f:
    f.write(jsonOutput)
    


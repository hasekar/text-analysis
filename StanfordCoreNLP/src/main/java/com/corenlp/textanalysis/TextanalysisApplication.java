package com.corenlp.textanalysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TextanalysisApplication {

	public static void main(String[] args) {
		SpringApplication.run(TextanalysisApplication.class, args);
	}
}

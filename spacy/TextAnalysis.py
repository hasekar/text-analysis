
# coding: utf-8

# In[7]:
from __future__ import unicode_literals, print_function

import nltk
import nltk.data
#nltk.download('vader_lexicon')
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import spacy 
from spacy import displacy


import plac
import random
from pathlib import Path
import spacy

from tqdm import tqdm # loading bar

nlp = spacy.load('en_core_web_sm')



# In[8]:


#READ FROM FILE
TEXT_ANALYSIS_FILE_NAME = "TextAnalytics.txt"
file= open(TEXT_ANALYSIS_FILE_NAME, "r")
file_content =  file.read()


# In[9]:


#SHOW FILE CONTENT
print (file_content)


# In[10]:



SENTENCE_OUTPUT_FILE_NAME = 'sentense.txt'

tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
file = open(TEXT_ANALYSIS_FILE_NAME, "r")
file_content =  file.read()
# print '\n'.join(tokenizer.tokenize(data))
out = open(SENTENCE_OUTPUT_FILE_NAME,"w+r")

# print 
# out.write('\n'.join(tokenizer.tokenize(file_content)))
# file2 = open(SENTENCE_OUTPUT_FILE_NAME, "r")



out.write('\n'.join(tokenizer.tokenize(file_content)))
output = out.read()
print (output)

# In[11]:


NEGATIVE_PHRASE_SENTENCE_SPACY = 'negativePhrase_spacy.txt'
SENTENCE_OUTPUT_FILE_NAME = 'sentense.txt'


  
sentenceFile = open(SENTENCE_OUTPUT_FILE_NAME, "r")
negativePhrase_spacy = open(NEGATIVE_PHRASE_SENTENCE_SPACY,"w")

sid = SentimentIntensityAnalyzer()

line = sentenceFile.readline()
cnt = 1
while line:
    list_of_words = line.split()
        
#    negatedWord = list_of_words[list_of_words.index('not') + 1]
    negativePhrase_spacy.write(line)
    print ('Found Negated Word')
    ss = sid.polarity_scores(line)
    if(ss['neg'] >= ss['pos'] ):
        print ('negative sentiment score')
        negativePhrase_spacy.write(line)
    else:
        print ('positive sentiment score')
        negativePhrase_spacy.write(line)

    line = sentenceFile.readline()
    cnt += 1


sentenceFile.close()
negativePhrase_spacy.close()


# In[12]:


# POS using NLTK


NEGATIVE_SENTENCE_OUTPUT_FILE_NAME = 'negativePhrase_spacy.txt'
CONCEPT2_OUTPUT_FILE_NAME = 'concepts.txt'


try:  
    negativeSenntenceFile_spacy = open(NEGATIVE_SENTENCE_OUTPUT_FILE_NAME,"r")
    conceptFile_nltk = open(CONCEPT2_OUTPUT_FILE_NAME,"w")


    line = negativeSenntenceFile_spacy.readline()
    
    cnt = 1
    
    while line:

        text = unicode(line)
        doc = nlp(text)
        
#        displacy.render(doc,style="ent", jupyter=True)

        concept = '';
        for token in doc:
#            print(token.text, token.dep_, token.head.text, token.head.pos_,[child for child in token.children])
            if(token.tag_ == 'NNP' or token.tag_ == 'NN' or token.tag_ == 'NNP' or token.tag_=='VBG' or token.tag_=='NN' or token.tag_ == 'RB' or token.tag_ == 'CD' or token.dep_ != 'neg'):
                if(token.tag_ != 'punct' and token.tag_ != 'CC' and token.tag_ != 'IN' and token.tag_ != 'DT' and token.text != '.'and token.text != '(' and token.text !=')' ):
                    concept = concept + token.text + ' + '
        
        print (concept)
        
        conceptFile_nltk.write(concept+'\n')

        print ("----------------")

        line = negativeSenntenceFile_spacy.readline()
        cnt += 1

finally:  
    negativeSenntenceFile_spacy.close()
    conceptFile_nltk.close()


# In[7]:





# In[8]:

#
## new entity label
#LABEL = 'SYSTEM_LIST'
#LOCATION = 'LOCATION'
#NEGATIVE_PHRASE = 'NEGATIVE_PHRASE'
#ACTIVITY = 'ACTIVITY'
#
#
## In[9]:
#
#
#TRAIN_DATA = [
#    ("Electronic data for chromatography systems are not archived in the QC laboratory to ensure adherence to data integrity principles", {
#        'entities': [(104, 118, 'SYSTEM_LIST')]
#    }),
#    ("dozens of analytical records with raw data file names for bonviva and xenical were found deleted in the recycle trash bin of stand", {
#        'entities': [(34, 42, 'SYSTEM_LIST')]
#    }),
#    ("The in-process verification of labeled vials for lot number and expiration date that is performed during vial labeling is not documented  with raw data in the Demonstration Batch Record.", {
#        'entities': [(158, 170, 'SYSTEM_LIST')]
#    }),
#    ("the qc laboratory system is lacking adequate controls to prevent raw data from being deleted and altered and to ensure electronic records are maintained and reviewed", {
#          'entities': [(85,92,'NEGATIVE_PHRASE'), (97, 105,'ACTIVITY')]
#    }),
#    ("the qc laboratory system is lacking adequate controls to prevent raw data from being deleted and altered and to ensure electronic records are maintained and reviewed", {
#          'entities': [(4, 13, 'LOCATION')]
#    }),
#    ("the qc laboratory system is lacking adequate controls to prevent raw data from being deleted and altered and to ensure electronic records are maintained and reviewed", {
#          'entities': [(97, 105,'ACTIVITY')]
#    })
#]
#
#
## In[10]:
#
#
## @plac.annotations(
##     model=("Model name. Defaults to blank 'en' model.", "option", "m", str),
##     new_model_name=("New model name for model meta.", "option", "nm", str),
##     output_dir=("Optional output directory", "option", "o", Path),
##     n_iter=("Number of training iterations", "option", "n", int))
#
#def main(model=None, new_model_name='system_list', output_dir=None, n_iter=20):
#    """Set up the pipeline and entity recognizer, and train the new entity."""
#    if model is not None:
#        nlp = spacy.load(model)  # load existing spaCy model
#        print("Loaded model '%s'" % model)
#    else:
#        nlp = spacy.blank('en')  # create blank Language class
#        print("Created blank 'en' model")
#    # Add entity recognizer to model if it's not in the pipeline
#    # nlp.create_pipe works for built-ins that are registered with spaCy
#    if 'ner' not in nlp.pipe_names:
#        ner = nlp.create_pipe('ner')
#        nlp.add_pipe(ner)
#    # otherwise, get it, so we can add labels to it
#    else:
#        ner = nlp.get_pipe('ner')
#
#    ner.add_label(LABEL)   # add new entity label to entity recognizer
#    ner.add_label(LOCATION)
#    ner.add_label(NEGATIVE_PHRASE)
#    ner.add_label(ACTIVITY)
#    if model is None:
#        optimizer = nlp.begin_training()
#    else:
#        # Note that 'begin_training' initializes the models, so it'll zero out
#        # existing entity types.
#        optimizer = nlp.entity.create_optimizer()
#
#    # get names of other pipes to disable them during training
#    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
#    with nlp.disable_pipes(*other_pipes):  # only train NER
#        for itn in range(n_iter):
#            random.shuffle(TRAIN_DATA)
#            losses = {}
#            for text, annotations in tqdm(TRAIN_DATA):
#                nlp.update([text], [annotations], sgd=optimizer, drop=0.35,
#                           losses=losses)
#            print(losses)
#
#    # test the trained model
#    test_text = "the qc laboratory system is lacking adequate controls to prevent raw data from being deleted and altered and to ensure electronic records are maintained and reviewed"
#    doc = nlp(test_text)
#    print("Entities in '%s'" % test_text)
#    for ent in doc.ents:
#        print(ent.label_, ent.text)
#
#    displacy.render(doc,style="ent", jupyter=True)
#
#
#    # save model to output directory
#    if output_dir is not None:
#        output_dir = Path(output_dir)
#        if not output_dir.exists():
#            output_dir.mkdir()
#        nlp.meta['name'] = new_model_name  # rename model
#        nlp.to_disk(output_dir)
#        print("Saved model to", output_dir)
#
#        # test the saved model
#        print("Loading from", output_dir)
#        nlp2 = spacy.load(output_dir)
#        doc2 = nlp2(test_text)
#        for ent in doc2.ents:
#            print(ent.label_, ent.text)
#
#
## In[11]:
#
#
#main()
#
#
## In[12]:


doc = nlp(u"the qc laboratory system is lacking adequate controls to prevent raw data from being deleted and altered and to ensure electronic records are maintained and reviewed")
for token in doc:
    print(token.text, token.dep_, token.head.text, token.head.pos_,
          [child for child in token.children])

